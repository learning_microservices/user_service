package postgres

import (
	
	"context"
	"fmt"
	"log"
	"time"
	"github.com/opentracing/opentracing-go"

	u "gitlab.com/micro/user_service/genproto/user"
)

func (r *UserRepo) CreateUser(ctx context.Context, user *u.UserRequest) (*u.UserResponse, error) {
	trace, ctx := opentracing.StartSpanFromContext(ctx, "CreateUser")
	defer trace.Finish()
	var res u.UserResponse
	err := r.db.QueryRow(`
		insert into 
			userdate(id, first_name, last_name, user_type, email, password, refresh_token) 
		values
			($1, $2, $3, $4, $5, $6, $7) 
		returning 
			id, first_name, last_name, user_type, email, created_at, updated_at, refresh_token`, user.Id, user.FirstName, user.LastName, user.UserType, user.Email, user.Password, user.RefreshToken).Scan(
		&res.Id, &res.FirstName, &res.LastName, &res.UserType, &res.Email, &res.CreatedAt, &res.UpdatedAt, &res.RefreshToken)

	if err != nil {
		log.Println("failed to create user")
		return &u.UserResponse{}, err
	}

	return &res, nil
}

func (r *UserRepo) GetUserById(ctx context.Context,user *u.IdRequest) (*u.UserResponse, error) {
	trace, ctx := opentracing.StartSpanFromContext(ctx, "GetUserById")
	defer trace.Finish()
	var res u.UserResponse
	err := r.db.QueryRow(`
		select 
			id, first_name, last_name, user_type, email, password, created_at, updated_at
		from 
			userdate 
		where id = $1 and deleted_at is null`, user.Id).Scan(
		&res.Id, &res.FirstName, &res.LastName, &res.UserType, &res.Email, &res.Password, &res.CreatedAt, &res.UpdatedAt)

	if err != nil {
		log.Println("failed to get user")
		return &u.UserResponse{}, err
	}

	return &res, nil
}

func (r *UserRepo) GetUserForClient(ctx context.Context,user_id *u.IdRequest) (*u.UserResponse, error) {
	trace, ctx := opentracing.StartSpanFromContext(ctx, "GetUserForClient")
	defer trace.Finish()
	var res u.UserResponse
	err := r.db.QueryRow(`
		select 
			id, first_name, last_name, email, created_at, updated_at 
		from 
			userdate
		where id = $1`, user_id.Id).Scan(
		&res.Id, &res.FirstName, &res.LastName, &res.Email, &res.CreatedAt, &res.UpdatedAt)

	if err != nil {
		log.Println("failed to get user for client")
		return &u.UserResponse{}, err
	}

	return &res, nil
}

func (r *UserRepo) GetAllUsers(ctx context.Context,req *u.AllUsersRequest) (*u.Users, error) {
	trace, ctx := opentracing.StartSpanFromContext(ctx, "GetAllUsers")
	defer trace.Finish()
	var res u.Users
	offset := (req.Page - 1) * req.Limit
	rows, err := r.db.Query(`
		select 
			id, first_name, last_name, user_type, email, created_at, updated_at 
		from 
			userdate 
		where 
			deleted_at is null 
		limit $1 offset $2`, req.Limit, offset)

	if err != nil {
		log.Println("failed to get all users")
		return &u.Users{}, err
	}

	for rows.Next() {
		temp := u.UserResponse{}

		err = rows.Scan(
			&temp.Id,
			&temp.FirstName,
			&temp.LastName,
			&temp.UserType,
			&temp.Email,
			&temp.CreatedAt,
			&temp.UpdatedAt,
		)
		if err != nil {
			log.Println("failed to scanning user")
			return &u.Users{}, err
		}

		res.Users = append(res.Users, &temp)
	}

	return &res, nil
}

func (r *UserRepo) SearchUsersByName(ctx context.Context,req *u.SearchUsers) (*u.Users, error) {
	trace, ctx := opentracing.StartSpanFromContext(ctx, "SearchUsersByName")
	defer trace.Finish()
	var res u.Users
	query := fmt.Sprint("select id, first_name, last_name, user_type, email, created_at, updated_at from userdate where first_name ilike '%" + req.FirstName + "%' and deleted_at is null")

	rows, err := r.db.Query(query)
	if err != nil {
		log.Println("failed to searching user")
		return &u.Users{}, err
	}

	for rows.Next() {
		temp := u.UserResponse{}

		err = rows.Scan(
			&temp.Id,
			&temp.FirstName,
			&temp.LastName,
			&temp.UserType,
			&temp.Email,
			&temp.CreatedAt,
			&temp.UpdatedAt,
		)
		if err != nil {
			log.Println("failed to searching user")
			return &u.Users{}, err
		}

		res.Users = append(res.Users, &temp)
	}

	return &res, nil
}

func (r *UserRepo) UpdateUser(ctx context.Context,user *u.UpdateUserRequest) error {
	trace, ctx := opentracing.StartSpanFromContext(ctx, "UpdateUser")
	defer trace.Finish()
	res, err := r.db.Exec(`
		update
			userdate
		set
			first_name = $1, last_name = $2, email = $3, password = $4, updated_at = $5
		where 
			id = $5`, user.FirstName, user.LastName, user.Email, user.Password, time.Now(), user.Id)

	if err != nil {
		log.Println("failed to update user")
		return err
	}

	fmt.Println(res.RowsAffected())
	return nil
}

func (r *UserRepo) DeleteUser(ctx context.Context,user *u.IdRequest) (*u.UserResponse, error) {
	trace, ctx := opentracing.StartSpanFromContext(ctx, "DeleteUser")
	defer trace.Finish()
	temp := u.UserResponse{}
	err := r.db.QueryRow(`
		update 
			userdate
		set 
			deleted_at = $1 
		where 
			id = $2 
		returning 
			id, first_name, last_name, email, created_at, updated_at`, time.Now(), user.Id).Scan(
		&temp.Id, &temp.FirstName, &temp.LastName, &temp.Email, &temp.CreatedAt, &temp.UpdatedAt)

	if err != nil {
		log.Println("failed to delete user")
		return &u.UserResponse{}, err
	}

	return &temp, nil
}

func (r *UserRepo) CheckField(ctx context.Context,req *u.CheckFieldReq) (*u.CheckFieldResp, error) {
	trace, ctx := opentracing.StartSpanFromContext(ctx, "CheckField")
	defer trace.Finish()
	query := fmt.Sprintf("SELECT 1 FROM userdate WHERE %s=$1", req.Field)
	res := &u.CheckFieldResp{}
	temp := -1
	err := r.db.QueryRow(query, req.Value).Scan(&temp)
	if err != nil {
		res.Exists = false
		return res, nil
	}
	if temp == 0 {
		res.Exists = true
	} else {
		res.Exists = false
	}
	return res, nil
}

func (r *UserRepo) Login(ctx context.Context,req *u.LoginReq) (*u.LoginResp, error) {
	trace, ctx := opentracing.StartSpanFromContext(ctx, "login")
	defer trace.Finish()
	var res u.LoginResp
	err := r.db.QueryRow(`
		select 
			id, first_name, last_name, email, password, created_at, updated_at
		from 
			userdate 
		where email = $1 and deleted_at is null`, req.Email).Scan(
		&res.Id, &res.FirstName, &res.LastName, &res.Email, &res.Password, &res.CreatedAt, &res.UpdatedAt)

	if err != nil {
		log.Println("failed to get user by email")
		return &u.LoginResp{}, err
	}

	return &res, nil
}

func (r *UserRepo) GetByEmail(ctx context.Context,req *u.EmailReq) (*u.UserResponse, error) {
	trace, ctx := opentracing.StartSpanFromContext(ctx, "GetByEmail")
	defer trace.Finish()
	var res u.UserResponse
	err := r.db.QueryRow(`
		select 
			id, first_name, last_name, email, password, created_at, updated_at
		from 
			userdate
		where email = $1 and deleted_at is null`, req.Email).Scan(
		&res.Id, &res.FirstName, &res.LastName, &res.Email, &res.Password, &res.CreatedAt, &res.UpdatedAt)

	if err != nil {
		log.Println("failed to get user by email")
		return &u.UserResponse{}, err
	}

	return &res, nil
}

func (r *UserRepo) CreateMod(ctx context.Context,req *u.IdRequest) (*u.Empty, error) {
	trace, ctx := opentracing.StartSpanFromContext(ctx, "CreateMod")
	defer trace.Finish()
	_, err := r.db.Exec(`update userdate set user_type = 'moderator' where id = $1 and deleted_at is null`, req.Id)
	if err != nil {
		return nil, err
	}
	return &u.Empty{}, nil
}
