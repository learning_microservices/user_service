package repo

import (
	"context"

	u "gitlab.com/micro/user_service/genproto/user"
)
 
type UserStoreI interface {
	CreateUser(context.Context,*u.UserRequest) (*u.UserResponse, error)
	GetUserById(context.Context,*u.IdRequest) (*u.UserResponse, error)
	GetUserForClient(context.Context,*u.IdRequest) (*u.UserResponse, error)
	GetAllUsers(context.Context,*u.AllUsersRequest) (*u.Users, error)
	SearchUsersByName(context.Context,*u.SearchUsers) (*u.Users, error)
	UpdateUser(context.Context,*u.UpdateUserRequest) error
	DeleteUser(context.Context,*u.IdRequest) (*u.UserResponse, error)
	CheckField(context.Context,*u.CheckFieldReq) (*u.CheckFieldResp, error)
	GetByEmail(context.Context,*u.EmailReq) (*u.UserResponse, error)
	CreateMod(context.Context,*u.IdRequest) (*u.Empty, error)
}