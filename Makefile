run:
	go run cmd/main.go

proto-gen:
	./scripts/gen-proto.sh

migrate_up:
	migrate -path migrations -database postgres://postgres:1234@localhost:5432/userdate -verbose up

migrate_down:
	migrate -path migrations -database postgres://postgres:1234@localhost:5432/userdate -verbose down

migrate_force:
	migrate -path migrations -database postgres://postgres:1234@localhost:5432/userdate -verbose force 0

test:
	go test ./...
.PHONY: start migrateup migratedown